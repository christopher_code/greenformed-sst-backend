import Joi from '@hapi/joi';

export default {
  transferFirst: Joi.object().keys({
    id: Joi.string().required().min(5),
    name: Joi.string().required().min(5),
    description: Joi.string().required().min(20),
    linkToBuy: Joi.string().optional().min(5),
    costPerProduct: Joi.number().required(),
    createdAt: Joi.number().required().min(12),
    categoryId: Joi.string().optional().min(5),
    typeOfCategory: Joi.string().optional().min(3),
    categoryCreatedAt: Joi.number().optional(),
  }),
  updateFields: Joi.object().keys({
    description: Joi.string().optional().min(20),
    linkToBuy: Joi.string().optional().min(5),
    costPerProduct: Joi.number().optional(),
  }),
};
