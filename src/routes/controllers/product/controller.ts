import express from 'express';
import { transferProductFrist, updateProductRelationships } from './service';
import { Products ,ProductPropertiesI } from "../../../database/models/Product";
import { Sellers } from "../../../database/models/Seller";
import { ReasonOfGHGEmsission } from "../../../database/models/GHG/reasonOfGHG";
import { singleGHGEmissions } from "../../../database/models/GHG/singleGHG";
import { totalGHGEmissions } from "../../../database/models/GHG/totalGHG";
import { SuccessResponse } from '../../../middleware/ApiResponse';
import { DataAlredayExistsError } from "../../../middleware/ApiError";
import asyncHandler from "../../../helpers/asyncFunc";
import validator from "../../../helpers/validator";
import schema from "./schema";
import { checkIfDataExists } from "../../../middleware/checkUniqueness";
import ProductService from "../../../database/entityService/ProductService";

const router = express.Router();

router.post('/transfer/first', 
    validator(schema.transferFirst),
    asyncHandler(async (req, res) => {
        const isNotUnique: boolean = await checkIfDataExists(req.body.id, req.body.name, "Products");
        if (isNotUnique) throw new DataAlredayExistsError('Transferred product does already exists.')
        let product: ProductPropertiesI = await transferProductFrist(req, res);
        new SuccessResponse('Product transeferred with success.', {product: product}).send(res);
        }
    )
);

router.put('/update/relationship/:productId/:changeId/:changeType', 
    asyncHandler(async (req, res) => {
        const exists: boolean = await checkIfDataExists(req.params.productId, "", "Products");
        if (!exists) throw new DataAlredayExistsError('Data not in SST');
        let product:ProductPropertiesI | any = await updateProductRelationships(req, res);
        new SuccessResponse('Transferred product successfuly related.', {
                product: product
            }).send(res);
        }
    )
)

router.delete('/delete/rel/:productId/:otherId/:relType', 
    asyncHandler(async (req, res) => {
        const exists: boolean = await checkIfDataExists(req.params.productId, "", "Products");
        if (!exists) throw new DataAlredayExistsError('Data not in SST');
        let alias: string = ''
        let model: any;
        if (req.params.relType === 'seller') {alias = 'CanBeBoughtFrom'; model = Sellers}
        else if (req.params.relType === 'reason') {alias = 'EmitsBecauseOf'; model = ReasonOfGHGEmsission}
        else if (req.params.relType === 'single') {alias = 'EmitsGHG'; model = singleGHGEmissions}
        else if (req.params.relType === 'total') {alias = 'EmitsInTotal'; model = totalGHGEmissions}
        else if (req.params.relType === 'alternative') {alias = 'HasAlternativeProduct'; model = Products}
        else if (req.params.relType === 'rawMaterial') {alias = 'MadeOfRawMaterial'; model = Products}
        let resp = await ProductService.deleteProductRelationship(alias, model, req, res);
        new SuccessResponse('Deletion of relationship was successful!', {resp}).send(res);
        }
    )
)

export default router;