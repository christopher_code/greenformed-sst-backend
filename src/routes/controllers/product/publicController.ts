import express from 'express';
import { getFullProduct, getProductCard, getThreeRandomProducts } from "./service";
import { SuccessResponse } from '../../../middleware/ApiResponse';
import asyncHandler from "../../../helpers/asyncFunc";
import { ProductPropertiesI } from "../../../database/models/Product";

const router = express.Router();

router.get('/get/:productId', 
    asyncHandler(async (req, res, next) => {
        let responseObj:ProductPropertiesI | any = await getFullProduct(req, res, next);
        new SuccessResponse('Full Product received successfuly', {
            product: responseObj.product.dataValues,
            seller: responseObj.productSeller,
            altternativeProduct: responseObj.productAlternative,
            productMadeOfRawMaterial: responseObj.productMadeOfRawMaterial,
            catgeory: responseObj.productCategory,
            totalEmission: responseObj.productTotalEmission,
            singleEmission: responseObj.productSingleEmission,
            emissionReason: responseObj.productEmissionReason,
        }).send(res);
        }
    )
)

router.get('/fetch/random', 
    asyncHandler(async (req, res) => {
        let repsonse:ProductPropertiesI[] | any = await getThreeRandomProducts();
        let product1: ProductPropertiesI = await getProductCard(repsonse[0], res);
        let product2: ProductPropertiesI = await getProductCard(repsonse[1], res);
        let product3: ProductPropertiesI = await getProductCard(repsonse[2], res);
        new SuccessResponse('Random Products received successfuly', [
            product1,
            product2,
            product3
        ]).send(res);
        }
    )
);

export default router;