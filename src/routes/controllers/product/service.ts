import { Products, ProductPropertiesI} from "../../../database/models/Product";
import { Sellers } from "../../../database/models/Seller";
import { Categories } from "../../../database/models/category";
import { ReasonOfGHGEmsission } from "../../../database/models/GHG/reasonOfGHG";
import { singleGHGEmissions } from "../../../database/models/GHG/singleGHG";
import { totalGHGEmissions } from "../../../database/models/GHG/totalGHG";
import { generateDate } from "../../../middleware/generateDate";
import { BadRequestError, InternalError } from "../../../middleware/ApiError"; 
import GeneralService from "../../../database/entityService/GeneralService";
import ProductService from "../../../database/entityService/ProductService";
import { Request, Response, NextFunction } from 'express';
import { transferCategoryFirst } from "../category/service";

export let transferProductFrist = async (req:Request, res: Response): Promise<ProductPropertiesI> => {
    try {
        let input = {
            id: req.body.id,
            name: req.body.name,
            description: req.body.description,
            linkToBuy: req.body.linkToBuy,
            costPerProduct: req.body.costPerProduct,
            createdAt: req.body.createdAt,
            transferredFirst: generateDate(),
        };

        let product: ProductPropertiesI | any = await GeneralService.transferNodeFirst(Products, input);

        if (req.body.typeOfCategory) {await transferCategoryFirst(req, res)}

        return product;
    }
    catch (err) {
        throw new BadRequestError();
    }  
};

export let updateProductRelationships = async (req:Request, res:Response): Promise<any> => {
    try {
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId, res);
        if (req.params.changeType === 'reasonGHG') {
            await product.relateTo({
                alias: "EmitsBecauseOf",
                where: {
                    id: req.params.changeId
                }
            })
        } else if (req.params.changeType === 'singleGHG') {
            await product.relateTo({
                alias: "EmitsGHG",
                where: {
                    id: req.params.changeId
                }
            })
        } else if (req.params.changeType === 'alternative') {
            await product.relateTo({
                alias: "HasAlternativeProduct",
                where: {
                    id: req.params.changeId
                }
            })
        } else if (req.params.changeType === 'rawmaterial') {
            await product.relateTo({
                alias: "MadeOfRawMaterial",
                where: {
                    id: req.params.changeId
                }
            })
        } else if (req.params.changeType === 'seller') {
            await product.relateTo({
                alias: "CanBeBoughtFrom",
                where: {
                    id: req.params.changeId
                }
            })
        } else {
            throw new BadRequestError('Relationship cannot be created due to wrong input.')
        }
        return product
    }
    catch (err) {
        throw new BadRequestError(err);
    }  
};

export let getProductCard = async (productId: string, res: Response): Promise<any> => {
    try {
        let product: ProductPropertiesI | any = await GeneralService.findNodeById(Products ,productId, res);;
        if (!product) {throw new BadRequestError("Product not found.");}                                                                                                                                                                                                                                                               
        let response: object = {
            product,
        }
        return response
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};

export let getFullProduct = async (req:Request, res:Response, next:NextFunction): Promise<any> => {
    try {
        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products ,req.params.productId, res);
        let productSeller: any = await ProductService.findProductAnyRelations(req.params.productId,
                                                                                Sellers,
                                                                                'CanBeBoughtFrom',
                                                                                next
                                                                            );   
        let productAlternative: any = await ProductService.findProductAnyRelations(req.params.productId,
                                                                                Products,
                                                                                'HasAlternativeProduct',
                                                                                next
                                                                            );     
        let productMadeOfRawMaterial: any = await ProductService.findProductAnyRelations(req.params.productId,
                                                                                Products,
                                                                                'MadeOfRawMaterial',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                                                                                                                                                 
        let productCategory: any = await ProductService.findProductAnyRelations(req.params.productId,
                                                                                Categories,
                                                                                'BelongsToCategory',
                                                                                next
                                                                            );    
        let productTotalEmission: any = await ProductService.findProductAnyRelations(req.params.productId,
                                                                                totalGHGEmissions,
                                                                                'EmitsInTotal',
                                                                                next
                                                                            );
        let productSingleEmission: any = await ProductService.findProductAnyRelations(req.params.productId,
                                                                                singleGHGEmissions,
                                                                                'EmitsGHG',
                                                                                next
                                                                            ); 
        let productEmissionReason: any = await ProductService.findProductAnyRelations(req.params.productId,
                                                                                ReasonOfGHGEmsission,
                                                                                'EmitsBecauseOf',
                                                                                next
                                                                            );                                                                                                                                                                                                                                                                   
        let responseObj: object = {
            product,
            productSeller,
            productAlternative,
            productMadeOfRawMaterial,
            productCategory,
            productTotalEmission,
            productSingleEmission,
            productEmissionReason,
        }
        return responseObj
    }
    catch (err) {
        throw new BadRequestError();
    }  
};

export let getThreeRandomProducts = async (): Promise<any> => {
    try {
        let totalSamples:number = await GeneralService.getTotalNumberOfSamples('Product');
        if (!totalSamples) {throw new BadRequestError("Total samples not found.");}
        let products:ProductPropertiesI | any = await GeneralService.findRandomNodes(totalSamples, Products);  
        if (!products) {throw new BadRequestError("Products not found.");}
        let data1 = products[1].dataValues.id;
        let data2 = products[products.length-1].dataValues.id;
        let data3 = products[0].dataValues.id;
        let response: any = new Array(
            data1,
            data2, 
            data3
        )                                                                                                                                                                                                                   
        return response
    }
    catch (err) {
        throw new InternalError("Something went wrong!");
    }  
};
