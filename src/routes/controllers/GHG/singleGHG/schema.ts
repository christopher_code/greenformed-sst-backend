import Joi from '@hapi/joi';

export default {
  transferFirst: Joi.object().keys({
    id: Joi.string().required().min(5),
    ghgName: Joi.string().required().min(2),
    emissionPerKG: Joi.number().required(),
    createdAt: Joi.number().required().min(12),
  }),
  updateFields: Joi.object().keys({
    emissionPerKG: Joi.number().required(),
  }),
};
