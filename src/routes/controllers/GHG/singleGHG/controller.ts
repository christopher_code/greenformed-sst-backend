import express from 'express';
import { transferSingleGHGFirst } from './service';
import { singleGHGEmissionPropertiesI } from "../../../../database/models/GHG/singleGHG";
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import { DataAlredayExistsError } from "../../../../middleware/ApiError";
import asyncHandler from "../../../../helpers/asyncFunc";
import validator from "../../../../helpers/validator";
import schema from "./schema";
import { checkIfDataExists } from "../../../../middleware/checkUniqueness";

const router = express.Router();

router.post('/transfer/first/relate/:productId', 
    validator(schema.transferFirst),
    asyncHandler(async (req, res) => {
        const isNotUnique: boolean = await checkIfDataExists(req.body.id, "", "Single");
        if (isNotUnique) throw new DataAlredayExistsError('Transferred Single GHG Data does already exists.')
        let single: singleGHGEmissionPropertiesI = await transferSingleGHGFirst(req, res);
        new SuccessResponse('Total GHG Data transeferred with success.', {singleGHG: single}).send(res);
        }
    )
);

export default router;