import { singleGHGEmissions, singleGHGEmissionPropertiesI } from "../../../../database/models/GHG/singleGHG";
import { Products, ProductPropertiesI } from "../../../../database/models/Product";
import { BadRequestError } from '../../../../middleware/ApiError';
import GeneralService from '../../../../database/entityService/GeneralService';
import { generateDate } from "../../../../middleware/generateDate";
import { Request, Response } from 'express';

export let transferSingleGHGFirst = async (req:Request, res: Response): Promise<singleGHGEmissionPropertiesI> => {
    try {
        let input = {
            id: req.body.id,
            ghgName: req.body.ghgName,
            emissionPerKG: req.body.emissionPerKG,
            createdAt: req.body.createdAt,
            transferredFirst: generateDate(),
        };

        let singleGHG: singleGHGEmissionPropertiesI = await GeneralService.transferNodeFirst(singleGHGEmissions, input);

        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId, res);
        await product.relateTo({
            alias: "EmitsGHG",
            where: {
                id: singleGHG.id
            }
        })
        return singleGHG;
    }
    catch (err) {
        throw new BadRequestError(err);
    }  
};
