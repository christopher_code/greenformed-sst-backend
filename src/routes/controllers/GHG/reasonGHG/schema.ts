import Joi from '@hapi/joi';

export default {
  transferFirst: Joi.object().keys({
    id: Joi.string().required().min(5),
    ghgName: Joi.string().required().min(2),
    reasonTitle: Joi.string().required().min(2),
    description: Joi.string().required().min(20),
    createdAt: Joi.number().required().min(12),
  }),
  updateFields: Joi.object().keys({
    reasonTitle: Joi.string().optional().min(2),
    description: Joi.string().optional().min(20),
  }),
};
