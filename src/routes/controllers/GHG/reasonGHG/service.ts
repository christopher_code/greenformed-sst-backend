import { ReasonOfGHGEmsission, reasonOfGGEmissionPropertiesI } from "../../../../database/models/GHG/reasonOfGHG";
import { Products, ProductPropertiesI } from "../../../../database/models/Product";
import { BadRequestError } from '../../../../middleware/ApiError';
import GeneralService from '../../../../database/entityService/GeneralService';
import { generateDate } from "../../../../middleware/generateDate";
import { Request, Response } from 'express';

export let transferReasonGHGFirst = async (req:Request, res: Response): Promise<reasonOfGGEmissionPropertiesI> => {
    try {
        let input = {
            id: req.body.id,
            ghgName: req.body.ghgName,
            reasonTitle: req.body.reasonTitle,
            description: req.body.description,
            createdAt: req.body.createdAt,
            transferredFirst: generateDate(),
        };

        let reasonGHG: reasonOfGGEmissionPropertiesI = await GeneralService.transferNodeFirst(ReasonOfGHGEmsission, input);

        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId, res);
        await product.relateTo({
            alias: "EmitsBecauseOf",
            where: {
                id: reasonGHG.id
            }
        })
        return reasonGHG;
    }
    catch (err) {
        throw new BadRequestError(err);
    }  
};
