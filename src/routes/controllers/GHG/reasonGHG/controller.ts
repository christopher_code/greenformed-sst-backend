import express from 'express';
import { reasonOfGGEmissionPropertiesI, ReasonOfGHGEmsission } from "../../../../database/models/GHG/reasonOfGHG";
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import { DataAlredayExistsError } from "../../../../middleware/ApiError";
import asyncHandler from "../../../../helpers/asyncFunc";
import validator from "../../../../helpers/validator";
import schema from "./schema";
import { checkIfDataExists } from "../../../../middleware/checkUniqueness";
import { transferReasonGHGFirst } from "./service";

const router = express.Router();

router.post('/transfer/first/relate/:productId', 
    validator(schema.transferFirst),
    asyncHandler(async (req, res) => {
            const isNotUnique: boolean = await checkIfDataExists(req.body.id, "", "Reason");
            if (isNotUnique) throw new DataAlredayExistsError('Transferred Reason of GHG Emission does already exists.')
            let reason: reasonOfGGEmissionPropertiesI = await transferReasonGHGFirst(req, res);
            new SuccessResponse('Reason GHG Data transeferred with success.', {reasonGHG: reason}).send(res);
        }
    )
);

export default router;