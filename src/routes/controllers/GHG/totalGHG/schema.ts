import Joi from '@hapi/joi';

export default {
  transferFirst: Joi.object().keys({
    id: Joi.string().required().min(5),
    ghgPerKG: Joi.number().required(),
    createdAt: Joi.number().required().min(12),
  }),
  updateFields: Joi.object().keys({
    ghgPerKG: Joi.number().required(),
  }),
};
