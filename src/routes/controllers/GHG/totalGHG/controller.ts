import express from 'express';
import { transferTotalGHGFirst } from './service';
import { totalGHGEmissionsPropertiesI } from "../../../../database/models/GHG/totalGHG";
import { SuccessResponse } from '../../../../middleware/ApiResponse';
import { DataAlredayExistsError } from "../../../../middleware/ApiError";
import asyncHandler from "../../../../helpers/asyncFunc";
import validator from "../../../../helpers/validator";
import schema from "./schema";
import { checkIfDataExists } from "../../../../middleware/checkUniqueness";

const router = express.Router();

router.post('/transfer/first/relate/:productId', 
    validator(schema.transferFirst),
    asyncHandler(async (req, res) => {
        const isNotUnique: boolean = await checkIfDataExists(req.body.id, "", "Total");
        if (isNotUnique) throw new DataAlredayExistsError('Transferred Total GHG Data does already exists.')
        let total: totalGHGEmissionsPropertiesI = await transferTotalGHGFirst(req, res);
        new SuccessResponse('Total GHG Data transeferred with success.', {totalGHG: total}).send(res);
        }
    )
);

export default router;