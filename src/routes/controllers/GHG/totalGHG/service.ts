import { totalGHGEmissions, totalGHGEmissionsPropertiesI } from "../../../../database/models/GHG/totalGHG";
import { Products, ProductPropertiesI } from "../../../../database/models/Product"
import { BadRequestError } from '../../../../middleware/ApiError';
import GeneralService from '../../../../database/entityService/GeneralService';
import { generateDate } from "../../../../middleware/generateDate";
import { Request, Response } from 'express';

export let transferTotalGHGFirst = async (req:Request, res: Response): Promise<totalGHGEmissionsPropertiesI> => {
    try {
        let input = {
            id: req.body.id,
            ghgPerKG: req.body.ghgPerKG,
            createdAt: req.body.createdAt,
            transferredFirst: generateDate(),
        };

        let totalGHG: any = GeneralService.transferNodeFirst(totalGHGEmissions, input);

        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId, res);
        await product.relateTo({
            alias: "EmitsInTotal",
            where: {
                id: totalGHG.id
            }
        })
        return totalGHG;
    }
    catch (err) {
        throw new BadRequestError(err);
    }  
};
