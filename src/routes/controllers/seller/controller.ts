import express from 'express';
import { transferSellerFirst } from './service';
import { SellerPropertiesI } from "../../../database/models/Seller";
import { SuccessResponse } from '../../../middleware/ApiResponse';
import { DataAlredayExistsError } from "../../../middleware/ApiError";
import asyncHandler from "../../../helpers/asyncFunc";
import validator from "../../../helpers/validator";
import schema from "./schema";
import { checkIfDataExists } from "../../../middleware/checkUniqueness";

const router = express.Router();

router.post('/transfer/first/relate/:productId', 
    validator(schema.transferFirst),
    asyncHandler(async (req, res) => {
        const isNotUnique: any = await checkIfDataExists(req.body.id, "", "Seller");
        if (isNotUnique) throw new DataAlredayExistsError('Transferred Seller does already exists.')
        let seller: SellerPropertiesI = await transferSellerFirst(req, res);
        new SuccessResponse('Seller transeferred with success.', {seller: seller}).send(res);
        }
    )
);

export default router;