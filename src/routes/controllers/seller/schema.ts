import Joi from '@hapi/joi';

export default {
  transferFirst: Joi.object().keys({
    id: Joi.string().required().min(5),
    name: Joi.string().required().min(3),
    link: Joi.string().required().min(5),
    createdAt: Joi.number().required().min(12),
  }),
  updateFields: Joi.object().keys({
    link: Joi.string().required().min(5),
  }),
};
