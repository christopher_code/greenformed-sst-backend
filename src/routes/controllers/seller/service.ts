import { Sellers, SellerPropertiesI } from "../../../database/models/Seller";
import { Products, ProductPropertiesI } from "../../../database/models/Product";
import { BadRequestError } from '../../../middleware/ApiError';
import GeneralService from '../../../database/entityService/GeneralService';
import { generateDate } from "../../../middleware/generateDate";
import { Request, Response } from 'express';

export let transferSellerFirst = async (req:Request, res: Response): Promise<SellerPropertiesI> => {
    try {
        let input = {
            id: req.body.id,
            name: req.body.name,
            link: req.body.link,
            createdAt: req.body.createdAt,
            transferredFirst: generateDate(),
        };

        let seller: SellerPropertiesI = await GeneralService.transferNodeFirst(Sellers, input);

        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.productId, res);
        await product.relateTo({
            alias: "CanBeBoughtFrom",
            where: {
                id: seller.id
            }
        })
        return seller;
    }
    catch (err) {
        throw new BadRequestError(err);
    }  
};
