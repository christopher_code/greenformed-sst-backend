import { Categories, CategoryPropertiesI } from "../../../database/models/category";
import { Products, ProductPropertiesI } from "../../../database/models/Product";
import { BadRequestError } from '../../../middleware/ApiError';
import GeneralService from '../../../database/entityService/GeneralService';
import { generateDate } from "../../../middleware/generateDate";
import { Request, Response } from 'express';

export let transferCategoryFirst = async (req:Request, res: Response): Promise<CategoryPropertiesI> => {
    try {
        let input = {
            id: req.body.categoryId,
            typeOfCategory: req.body.typeOfCategory,
            createdAt: req.body.categoryCreatedAt,
            transferredFirst: generateDate(),
        };

        let category: CategoryPropertiesI = await GeneralService.transferNodeFirst(Categories, input);

        let product:ProductPropertiesI | any = await GeneralService.findNodeById(Products, req.params.id, res);
        await product.relateTo({
            alias: "BelongsToCategory",
            where: {
                id: category.id
            }
        })
        return category;
    }
    catch (err) {
        throw new BadRequestError(err);
    }  
};
