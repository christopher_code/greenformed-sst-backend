import Joi from '@hapi/joi';

export default {
  transferFirst: Joi.object().keys({
    categoryId: Joi.string().required().min(5),
    typeOfCategory: Joi.string().required().min(3),
    categoryCreatedAt: Joi.number().required().min(12),
  }),
};
