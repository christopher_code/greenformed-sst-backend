import express from 'express';
import { transferCategoryFirst } from './service';
import { CategoryPropertiesI } from "../../../database/models/category";
import { SuccessResponse } from '../../../middleware/ApiResponse';
import asyncHandler from "../../../helpers/asyncFunc";
import validator from "../../../helpers/validator";
import schema from "./schema";

const router = express.Router();

router.post('/transfer/first/relate/:productId', 
    validator(schema.transferFirst),
    asyncHandler(async (req, res) => {
        let category: CategoryPropertiesI = await transferCategoryFirst(req, res);
        new SuccessResponse('Category transeferred with success.', {category: category}).send(res);
        }
    )
);

export default router;