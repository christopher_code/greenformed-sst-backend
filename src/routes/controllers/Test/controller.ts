import express from 'express';
import { SuccessResponse } from '../../../middleware/ApiResponse';
import asyncHandler from "../../../helpers/asyncFunc";
import { decryptData } from "../../../middleware/decryptData";

const router = express.Router();

router.post('/encrypted', 
    asyncHandler(async (req, res) => {
        let data: any = await decryptData(req);
        new SuccessResponse('Encrypted Data was read successfully.', {data: data}).send(res);
        }
    )
);

export default router;