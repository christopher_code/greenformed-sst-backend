import { Request, Response } from 'express';
import { BadRequestError } from "../../../middleware/ApiError";
import { generateDate } from "../../../middleware/generateDate";
import GeneralService from "../../../database/entityService/GeneralService";

export let updateNodeFields = async (Model: any, fetchId: string, req:Request, res:Response): Promise<any> => {
    try {
        let node:any = await GeneralService.findNodeById(Model, fetchId, res);
        if (req.params.model === 'product') {
            if (req.body.costPerProduct) {
                node.costPerProduct = req.body.costPerProduct;
            } else if (req.body.description) {
                node.description = req.body.description;
            } else if (req.body.linkToBuy) {
                node.linkToBuy = req.body.linkToBuy;
            } else {
                throw new BadRequestError("Data provided is not correct.")
            }
        } else if (req.params.model === 'reason') {
            if (req.body.description) {
                node.description = req.body.description;
            } else {
                throw new BadRequestError("Data provided is not correct.")
            }
        } else if (req.params.model === 'single') {
            if (req.body.emissionPerKG) {
                node.emissionPerKG = req.body.emissionPerKG;
            } else {
                throw new BadRequestError("Data provided is not correct.")
            }
        } else if (req.params.model === 'total') {
            if (req.body.ghgPerKG) {
                node.ghgPerKG = req.body.ghgPerKG;
            } else {
                throw new BadRequestError("Data provided is not correct.")
            }
        } else if (req.params.model === 'seller') {
            if (req.body.link) {
                node.link = req.body.link;
            } else {
                throw new BadRequestError("Data provided is not correct.")
            }
        } else {throw new BadRequestError("Data provided is not correct.")}
        node.lastUpdate = generateDate();
        await node.save();
        return node;
    }
    catch (err) {
        throw new BadRequestError(err);
    }  
};
