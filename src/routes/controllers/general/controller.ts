import express from 'express';
import { Products } from "../../../database/models/Product";
import { Sellers } from "../../../database/models/Seller";
import { ReasonOfGHGEmsission } from "../../../database/models/GHG/reasonOfGHG";
import { singleGHGEmissions } from "../../../database/models/GHG/singleGHG";
import { totalGHGEmissions } from "../../../database/models/GHG/totalGHG";
import { SuccessResponse } from '../../../middleware/ApiResponse';
import { BadRequestError } from "../../../middleware/ApiError";
import asyncHandler from "../../../helpers/asyncFunc";
import GeneralService from "../../../database/entityService/GeneralService";
import { updateNodeFields } from "./service";

const router = express.Router();

router.put('/update/fields/:fetchId/:model', 
    asyncHandler(async (req, res) => {
        let model: any; 
        if (req.params.model === 'product') {model=Products}
        else if (req.params.model === 'seller') {model=Sellers}
        else if (req.params.model === 'reason') {model=ReasonOfGHGEmsission}
        else if (req.params.model === 'total') {model=totalGHGEmissions}
        else if (req.params.model === 'single') {model=singleGHGEmissions}
        let node:any = await updateNodeFields(model ,req.params.fetchId, req, res);
        new SuccessResponse('Transferred data updated successfuly', {
                node: node
            }).send(res);
        }
    )
)

router.delete('/delete/node/:deleteId/:model', 
    asyncHandler(async (req, res) => {
        let model: any;
        if (req.params.model === 'product') {model = Products}
        else if (req.params.model === 'seller') {model = Sellers}
        else if (req.params.model === 'reason') {model = ReasonOfGHGEmsission}
        else if (req.params.model === 'single') {model = singleGHGEmissions}
        else if (req.params.model === 'total') {model = totalGHGEmissions}
        let resp: boolean = await GeneralService.deleteNode(model, req);
        if (!resp) {throw new BadRequestError("Product deletion was not successfull.")}
        new SuccessResponse('Deletion of product node was successful!', {}).send(res);
        }
    )
)

export default router;