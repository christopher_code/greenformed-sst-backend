import express from 'express';

const verifyToken = require('../middleware/VerifyAdminToken');

import product from "./controllers/product/controller";
import publicProduct from "./controllers/product/publicController";

import totalGHG from "./controllers/GHG/totalGHG/controller";

import singleGHG from "./controllers/GHG/singleGHG/controller";

import reasonGHG from "./controllers/GHG/reasonGHG/controller";

import seller from "./controllers/seller/controller";

import general from "./controllers/general/controller";

import test from "./controllers/Test/controller";

const router = express.Router();

router.use('/product', verifyToken, product);
router.use('/o/product', publicProduct);

router.use('/ghg/total', verifyToken, totalGHG);

router.use('/ghg/single', verifyToken, singleGHG);

router.use('/ghg/reason', verifyToken, reasonGHG);

router.use('/seller', verifyToken, seller);

router.use('/general', verifyToken, general);

router.use('/test', verifyToken, test);

export default router;