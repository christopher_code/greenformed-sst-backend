import { neogma } from "../../app";
import { QueryBuilder, QueryRunner } from 'neogma';
import { Products, ProductPropertiesI } from '../models/Product';
import { BadRequestError, InternalError } from '../../middleware/ApiError';
import { Request, Response, NextFunction } from 'express';

export default class ProductService {

    public static async deleteProduct(req: Request, res:Response):Promise<any> {
        try {
            await Products.delete({
                where: {
                    id: req.params.productId
                },
                detach: true,
            });
            return true;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async deleteProductRelationship(alias: any, OtherModel: any, req: Request, res:Response):Promise<any> {
        try {
            await new QueryBuilder()
            .match({
                related: [
                    {
                        model: Products,
                        where: {
                            id: req.params.productId,
                        },
                    },
                    {
                        ...Products.getRelationshipByAlias(alias),
                        identifier: 'r',
                    },
                    {
                        model: OtherModel,
                         where: {
                             id: req.params.otherId
                          }
                    },
                ],
            })
            .delete({
                identifiers: 'r'
            }).run(neogma.queryRunner);

            return 'success';
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findProductAnyRelations(id: string, model: any, alias: any, next: NextFunction):Promise<any> {
        try {
            const getProductRelationship = await new QueryBuilder()
            .match({
                related: [
                    {
                        model: Products,
                        where: {
                            id: id,
                        },
                        identifier: 'products',
                    },
                    {
                        ...Products.getRelationshipByAlias(alias),
                        identifier: alias,
                    },
                    {
                        model: model,
                        identifier: 'model',
                    },
                ],
            })
            .return([
                'products',
                'model',
            ])
            .run(neogma.queryRunner);

            if (!getProductRelationship) next();
    
            const data = QueryRunner.getResultProperties<any>(
                getProductRelationship,
                'model',
            );

            return data;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findRandomProducts(total: number):Promise<ProductPropertiesI | any> {
        try {
            let randomNum: number = Math.floor(Math.random() * (total-3))
            if (randomNum < 0) {randomNum = 0};
            const products:ProductPropertiesI | any = await Products.findMany({where: {}, 
                skip: randomNum,
                // limit: 3,
                order: [['createdAt', 'DESC']],
            });

            if (!products) throw new BadRequestError('Product does not exists!');
            return products;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }
}
