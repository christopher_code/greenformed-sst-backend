import { neogma } from "../../app";
import { QueryRunner } from 'neogma';
import { BadRequestError, InternalError } from "../../middleware/ApiError";
import { Request, Response } from 'express';

export default class GeneralService {

    public static async transferNodeFirst(model: any, input: any): Promise<any> {
        try {
            let nodeCreated:any = await model.createOne(input);
            return nodeCreated;
        }
        catch (err) {
            throw new BadRequestError(err);
        }  
    };

    public static async findNodeById(Model: any, id: string, res:Response):Promise<any> {
        try {
            const node:any = await Model.findOne({
                where: {
                    id: id,
                },
            });

            if (!node) throw new BadRequestError('Data does not exists!');
            return node;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async findRandomNodes(total: number, model: any):Promise<any> {
        try {
            let randomNum: number = Math.floor(Math.random() * (total-3))
            if (randomNum < 0) {randomNum = 0};
            const nodes:any = await model.findMany({where: {}, 
                skip: randomNum,
                // limit: 3,
                order: [['createdAt', 'DESC']],
            });

            if (!nodes) throw new BadRequestError('Product does not exists!');
            return nodes;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async deleteNode(Model: any, req: Request):Promise<any> {
        try {
            await Model.delete({
                where: {
                    id: req.params.deleteId
                },
                detach: true,
            });
            return true;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }

    public static async getTotalNumberOfSamples(model: string):Promise<any> {
        try {
            const queryRunner = new QueryRunner({
                driver: neogma.driver,
                logger: console.log
            });

            const result = await queryRunner.run(
                `MATCH (n:${model})
                    RETURN count(n) as count`,
            );

            if (!result) throw new BadRequestError('Index of queried does not exist!');
            let zwi: any = result.records[0];
            return zwi._fields[0].low;
        } catch (err) {
            throw new InternalError('Something went wrong.');
        }
    }
}
