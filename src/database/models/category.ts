import { neogma } from '../../app';
import { ModelFactory, NeogmaInstance, ModelRelatedNodesI } from 'neogma';

export type CategoryPropertiesI = {
    id: string,
    typeOfCategory: string;
    createdAt: number;
    transferredFirst: number,
    lastUpdate?: number,
};

export interface CategoryRelatedNodesI {
    IsSubcategoryOf: ModelRelatedNodesI<typeof Categories, CategoryPropertiesI>;
    IsSuperCategoryOf: ModelRelatedNodesI<typeof Categories, CategoryPropertiesI>;
}

export type CategoryInstance = NeogmaInstance<
    CategoryPropertiesI,
    CategoryRelatedNodesI
>;

export let Categories = ModelFactory<CategoryPropertiesI, CategoryRelatedNodesI>(
    {
        label: 'Category',
        primaryKeyField: 'typeOfCategory',
        schema: {
            id: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            typeOfCategory : {
                type: 'string',
                minLength: 5,
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
            transferredFirst: {
                type: 'number',
                required: true,
            },
            lastUpdate: {
                type: 'number',
                required: false,
            },
        },
        relationships: {
            IsSubcategoryOf: {
                model: 'self',
                direction: 'out',
                name: 'IS_SUB_CATEGORY_OF',
            },
            IsSuperCategoryOf: {
                model: 'self',
                direction: 'out',
                name: 'IS_SUPER_CATEGORY_OF',
            },
        },
    },
    neogma,
);
