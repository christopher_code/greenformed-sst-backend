import { neogma } from '../../app';
import { ModelFactory, NeogmaInstance, ModelRelatedNodesI } from 'neogma';
import { Categories, CategoryPropertiesI } from './category';

export type SellerPropertiesI = {
    id: string, 
    name: string, 
    link: string,
    createdAt: number,
    transferredFirst: number,
    lastUpdate?: number,
};

export interface SellerRelatedNodesI {
    BelongsToCategory: ModelRelatedNodesI<typeof Categories, CategoryPropertiesI>;
}

export type SellerInstance = NeogmaInstance<
    SellerPropertiesI,
    SellerRelatedNodesI
>;

export const Sellers = ModelFactory<SellerPropertiesI, SellerRelatedNodesI>(
    {
        label: 'Seller',
        primaryKeyField: 'name',
        schema: {
            name : {
                type: 'string',
                minLength: 5,
                required: true,
            },
            id: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            link: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
            transferredFirst: {
                type: 'number',
                required: true,
            },
            lastUpdate: {
                type: 'number',
                required: false,
            },
        },
        relationships: {
            BelongsToCategory: {
                model: Categories,
                direction: 'out',
                name: 'BELONGS_TO_CATEGORY',
            },
        },
    },
    neogma,
);