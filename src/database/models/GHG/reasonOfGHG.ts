import { neogma } from '../../../app';
import { ModelFactory, NeogmaInstance } from 'neogma';

export type reasonOfGGEmissionPropertiesI = {
    id: string,
    ghgName: string,
    reasonTitle: string,
    description: string,
    createdAt: number,
    transferredFirst: number,
    lastUpdate?: number,
};

export interface reasonOfGHGEmissionRelatedNodesI {
}

export type reasonOfGHGEmissionsInstance = NeogmaInstance<
reasonOfGGEmissionPropertiesI,
reasonOfGHGEmissionRelatedNodesI
>;

export const ReasonOfGHGEmsission = ModelFactory<reasonOfGGEmissionPropertiesI, reasonOfGHGEmissionRelatedNodesI>(
    {
        label: 'ReasonOfGHGEmsission',
        primaryKeyField: 'id',
        schema: { 
            id: {
                type: 'string',
                minLength: 5,
                required: true, 
            },
            ghgName: {
                type: 'string',
                minLength: 3,
                required: true, 
            },
            reasonTitle: {
                type: 'string',
                minLength: 5,
                required: true, 
            },
            description: {
                type: 'string',
                minLength: 20,
                required: true, 
            },
            createdAt: {
                type: 'number',
                required: true,
            },
            transferredFirst: {
                type: 'number',
                required: true,
            },
            lastUpdate: {
                type: 'number',
                required: false,
            },
        },
    },
    neogma,
);