import { neogma } from '../../../app';
import { ModelFactory, NeogmaInstance } from 'neogma';

export type singleGHGEmissionPropertiesI = {
    id: string,
    ghgName: string,
    emissionPerKG: number,
    createdAt: number,
    transferredFirst: number,
    lastUpdate?: number,
};

export interface singleGHGEmissionRelatedNodesI {}

export type singleGHGEmissionsInstance = NeogmaInstance<
singleGHGEmissionPropertiesI,
singleGHGEmissionRelatedNodesI
>;

export const singleGHGEmissions = ModelFactory<singleGHGEmissionPropertiesI, singleGHGEmissionRelatedNodesI>(
    {
        label: 'SingleGHGEmsissionPerKG',
        primaryKeyField: 'id',
        schema: { 
            id: {
                type: 'string',
                minLength: 5,
                required: true, 
            },
            ghgName: {
                type: 'string',
                minLength: 3,
                required: true, 
            },
            emissionPerKG: {
                type: 'number',
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
            transferredFirst: {
                type: 'number',
                required: true,
            },
            lastUpdate: {
                type: 'number',
                required: false,
            },
        },
    },
    neogma,
);