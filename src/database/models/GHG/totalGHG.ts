import { neogma } from '../../../app';
import { ModelFactory, NeogmaInstance } from 'neogma';

export type totalGHGEmissionsPropertiesI = {
    id: string,
    ghgPerKG: number,
    createdAt: number,
    transferredFirst: number,
    lastUpdate?: number,
};

export interface totalGHGEmissionsRelatedNodesI {}

export type totalGHGEmissionsInstance = NeogmaInstance<
    totalGHGEmissionsPropertiesI,
    totalGHGEmissionsRelatedNodesI
>;

export const totalGHGEmissions = ModelFactory<totalGHGEmissionsPropertiesI, totalGHGEmissionsRelatedNodesI>(
    {
        label: 'TotoalGHGEmissionsPerKG',
        primaryKeyField: 'id',
        schema: { 
            id: {
                type: 'string',
                minLength: 5,
                required: true, 
            },
            ghgPerKG: {
                type: 'number',
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
            transferredFirst: {
                type: 'number',
                required: true,
            },
            lastUpdate: {
                type: 'number',
                required: false,
            },
        },
    },
    neogma,
);