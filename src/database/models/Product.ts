import { neogma } from '../../app';
import { ModelFactory, NeogmaInstance, ModelRelatedNodesI } from 'neogma';
import { Categories, CategoryPropertiesI } from './category';
import { Sellers, SellerPropertiesI } from "./Seller";
import { totalGHGEmissions, totalGHGEmissionsPropertiesI } from "./GHG/totalGHG";
import { singleGHGEmissions, singleGHGEmissionPropertiesI } from "./GHG/singleGHG";
import { ReasonOfGHGEmsission, reasonOfGGEmissionPropertiesI } from "./GHG/reasonOfGHG";

export type ProductPropertiesI = {
    id: string, 
    name: string, 
    description: string,
    linkToBuy?: string, 
    costPerProduct?: number,
    createdAt: number,
    transferredFirst: number,
    lastUpdate?: number,
};

export interface ProductRelatedNodesI {
    HasAlternativeProduct: ModelRelatedNodesI<typeof Products, ProductPropertiesI>;
    MadeOfRawMaterial: ModelRelatedNodesI<typeof Products, ProductPropertiesI>;
    BelongsToCategory: ModelRelatedNodesI<typeof Categories, CategoryPropertiesI>;
    CanBeBoughtFrom: ModelRelatedNodesI<typeof Sellers, SellerPropertiesI>;
    EmitsInTotal: ModelRelatedNodesI<typeof totalGHGEmissions, totalGHGEmissionsPropertiesI>;
    EmitsGHG: ModelRelatedNodesI<typeof singleGHGEmissions, singleGHGEmissionPropertiesI>;
    EmitsBecauseOf: ModelRelatedNodesI<typeof ReasonOfGHGEmsission, reasonOfGGEmissionPropertiesI>;
}

export type ProductInstance = NeogmaInstance<
    ProductPropertiesI,
    ProductRelatedNodesI
>;

export let Products = ModelFactory<ProductPropertiesI, ProductRelatedNodesI>(
    {
        label: 'Product',
        primaryKeyField: 'name',
        schema: {
            name : {
                type: 'string',
                minLength: 2,
                required: true,
            },
            id: {
                type: 'string',
                minLength: 5,
                required: true,
            },
            description: {
                type: 'string',
                minLength: 20,
                required: true,
            },
            linkToBuy: {
                type: 'string',
                minLength: 5,
            },
            costPerProduct: {
                type: 'number',
                required: true,
            },
            createdAt: {
                type: 'number',
                required: true,
            },
            transferredFirst: {
                type: 'number',
                required: true,
            },
            lastUpdate: {
                type: 'number',
                required: false,
            },
        },
        relationships: {
            HasAlternativeProduct: {
                model: "self",
                direction: 'out',
                name: 'HAS_ALTERNATIVE_PRODUCT',
            },
            MadeOfRawMaterial: {
                model: "self",
                direction: 'out',
                name: 'MADE_OF_RAW_MATERIAL',
            },
            BelongsToCategory: {
                model: Categories,
                direction: 'out',
                name: 'BELONGS_TO_CATEGORY',
            },
            CanBeBoughtFrom: {
                model: Sellers,
                direction: 'out',
                name: 'CAN_BE_BOUGHT_FROM',
            },
            EmitsInTotal: {
                model: totalGHGEmissions,
                direction: 'out',
                name: 'EMITS_IN_TOTAL',
            },
            EmitsGHG: {
                model: singleGHGEmissions,
                direction: 'out',
                name: 'EMITS',
            },
            EmitsBecauseOf: {
                model: ReasonOfGHGEmsission,
                direction: 'out',
                name: 'EMITS_BECAUSE_OF',
            },
        },
    },
    neogma,
);