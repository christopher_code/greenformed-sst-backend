const NodeRSA = require('node-rsa');
import { privateKey } from "../../config"

export let decryptData = async(req: any): Promise<any> => {
    const key = new NodeRSA({b: 512});
    let privateKeyImported = key.importKey(privateKey, 'private');
    const decrypted = JSON.parse(privateKeyImported.decrypt(req.body.data, 'utf8'));

    return decrypted;
}