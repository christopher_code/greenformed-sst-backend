import  jwt from 'jsonwebtoken';
import { adminToken, adminMail } from '../../config';
import { AuthFailureError} from "./ApiError";

function verifyToken(req:any, res:any, next:any): any {
  let token = req.headers.authorization.split(' ')[1];
  if (!token)
  res.status(403).send('No token provided')
    
  jwt.verify(token, adminToken, function(err:any, decoded:any) {
    if (err)
        res.status(403).send('Failed to authentificate token')
    
    req.userMail = decoded.theUserMail;
    if (decoded.theUserMail !== adminMail) {throw new AuthFailureError('Wrong credentials!')}
    next();
  });
}

module.exports = verifyToken;