import { BadRequestError } from './ApiError';
import { Categories } from '../database/models/category';
import { Sellers } from '../database/models/Seller';
import { Products } from '../database/models/Product';
import { totalGHGEmissions } from "../database/models/GHG/totalGHG";
import { singleGHGEmissions } from "../database/models/GHG/singleGHG";
import { ReasonOfGHGEmsission } from "../database/models/GHG/reasonOfGHG";

export async function checkIfDataExists(input: string, input2: string, model: string): Promise<any> {
    try {
        let approve: any;
        if (model === 'Categories') {
            approve = await Categories.findOne({
                where: {
                    id: input
                },
            });
        } else if (model === 'Sellers') {
            approve = await Sellers.findOne({
                where: {
                    id: input
                },
            });
        } else if (model === 'Products') {
            approve = await Products.findOne({
                where: {
                    id: input
                },
            });
            if (!approve) {
                approve = await Products.findOne({
                    where: {
                        name: input2
                    },
                });
            }
        } else if (model === 'Total') {
            approve = await totalGHGEmissions.findOne({
                where: {
                    id: input
                },
            });
        } else if (model === 'Single') {
            approve = await singleGHGEmissions.findOne({
                where: {
                    id: input
                },
            });
        } else if (model === 'Reason') {
            approve = await ReasonOfGHGEmsission.findOne({
                where: {
                    id: input
                },
            });
        }
        if (approve) {
            return true;
        } else {
            return false
        }
    }
    catch (err) {
        throw new BadRequestError();
    }  
}