import dotenv from 'dotenv';
dotenv.config({ path: './tests/.env.test' });

export const authAdminToken: any = process.env.ADMIN_JWT;