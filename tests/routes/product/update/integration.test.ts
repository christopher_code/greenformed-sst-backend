jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Product Update';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;

describe('Integration: Test create product', () => {
  const endpointTransferProduct = '/api/product/transfer/first';
  const endpointUpdateProduct = '/api/general/update/fields/a1234567890abc/product';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Error: Update Product invalid field', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointTransferProduct).send({
            id: "a1234567890abc1",
            name: PRODUCTNAME + 'a',
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
            createdAt: CREATEDAT,
        }),
        authAdminToken
      );
    
    expect(responseProductCreation.status).toBe(200);

    const responseProductUpdate = await addAuthHeaders(
        request.put(endpointUpdateProduct + '1').send({
            name: 'test name',
        }),
        authAdminToken
      );

    expect(responseProductUpdate.status).toBe(400);
  });

  it('Error: Auth error - wrong token', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointTransferProduct).send({
            id: "a1234567890abc2",
            name: PRODUCTNAME + 'ab',
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
            createdAt: CREATEDAT,
        }),
        authAdminToken
      );
    
    expect(responseProductCreation.status).toBe(200);

    const responseProductUpdate = await addAuthHeaders(
        request.put(endpointUpdateProduct + '2').send({
          linkToBuy: LINKTOBUY + 'abc',
        }),
        '123abc'
      );

    expect(responseProductUpdate.status).toBe(403);
  });

});