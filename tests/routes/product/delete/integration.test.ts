jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Product Delete';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;

describe('Integration: Test create product', () => {
  const endpointTransferProduct = '/api/product/transfer/first';
  const endpointDeleteProduct = '/api/general/delete/node/a1234567890abc123/product';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Delete Product after transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointTransferProduct).send({
            id: "a1234567890abc123",
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
            createdAt: CREATEDAT,
        }),
        authAdminToken
      );
    
    expect(responseProductCreation.status).toBe(200);

    const responseProductDelete = await addAuthHeaders(
        request.delete(endpointDeleteProduct).send({}),
        authAdminToken
      );

    expect(responseProductDelete.status).toBe(200);
  });

  it('Error: Delete Product with wrong product id', async () => {
    const responseProductDelete = await addAuthHeaders(
        request.delete(endpointDeleteProduct + "1").send({}),
        authAdminToken
      );

    expect(responseProductDelete.status).toBe(500);
  });

  it('Error: Delete Product with wrong product id', async () => {
    const responseProductDelete = await addAuthHeaders(
        request.delete(endpointDeleteProduct).send({}),
        '123'
      );

    expect(responseProductDelete.status).toBe(403);
  });

});