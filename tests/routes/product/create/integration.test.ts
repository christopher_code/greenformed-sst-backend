jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Product';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;

describe('Integration: Test create product', () => {
  const endpointTransferProduct = '/api/product/transfer/first'
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Create Product after login', async () => {
    
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointTransferProduct).send({
            id: "ssdfsdfsdf3dddd3434534534",
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
            createdAt: CREATEDAT,
        }),
        authAdminToken
      );
    
    expect(responseProductCreation.status).toBe(200);
    expect(responseProductCreation.body.message).toMatch(/Success/i);
  });

  it('Error: Transfer Product with same id and name', async () => {
    await addAuthHeaders(
        request.post(endpointTransferProduct).send({
            id: "ssdfsdfsdf3dddd3434534534",
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
            createdAt: CREATEDAT,
        }),
        authAdminToken
      );

      const responseProductCreation = await addAuthHeaders(
        request.post(endpointTransferProduct).send({
            id: "ssdfsdfsdf3dddd3434534534",
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
            createdAt: CREATEDAT,
        }),
        authAdminToken
      );
    
    expect(responseProductCreation.status).toBe(406);
    expect(responseProductCreation.body.data).not.toBeDefined();
  });

  it('Error: Transfer Product with wrong id type', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointTransferProduct).send({
            id: 123,
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
            createdAt: CREATEDAT,
        }),
        authAdminToken
      );
    
    expect(responseProductCreation.status).toBe(400);
    expect(responseProductCreation.body.data).not.toBeDefined();
  });

});