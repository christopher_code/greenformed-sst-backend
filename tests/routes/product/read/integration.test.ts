jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders, addHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Product';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;

describe('Integration: Test create product', () => {
  const endpointTransferProduct = '/api/product/transfer/first';
  const endpointGetProduct = '/api/o/product/get/ssdfsdfsdf3dddd3434534534';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Create Product after login and read data', async () => {
    const responseProductCreation = await addAuthHeaders(
        request.post(endpointTransferProduct).send({
            id: "ssdfsdfsdf3dddd3434534534",
            name: PRODUCTNAME,
            description: DESCRIPTIONPRODUCT,
            linkToBuy: LINKTOBUY,
            costPerProduct: COSTPERPRODUCT,
            createdAt: CREATEDAT,
        }),
        authAdminToken
      );
    
    expect(responseProductCreation.status).toBe(200);

    const responseFetch = await addHeaders(
        request.get(endpointGetProduct)
    );

    expect(responseFetch.status).toBe(200);
    expect(responseFetch.body.data).toBeDefined();
    expect(responseFetch.body.data.product).toBeDefined();
    expect(responseFetch.body.data.product.name).toEqual(PRODUCTNAME);
  });
});