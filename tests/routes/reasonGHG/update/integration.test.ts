jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Reason Update';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;
const REASONCREATEDAT = 1212434342312323;
const REASONNAME = 'Transportation';
const REASONTITLE = 'Transportation 1';
const REASONDESCR = "lorem ipsum dolor sit amet, lorem ipsum dolor sit amet,lorem ipsum dolor sit amet,"

describe('Integration: Test update reason ghg', () => {
  const endpointTransferReason = '/api/ghg/reason/transfer/first/relate/';
  const endpointUpdateReason = '/api/general/update/fields/idReasonGHG/reason';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Update Reason GHG after Product transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "1234abc",
          name: PRODUCTNAME,
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferReason + '1234abc').send({
              id:  "idReasonGHG",
              ghgName: REASONNAME,
              reasonTitle: REASONTITLE,
              description: REASONDESCR,
              createdAt: REASONCREATEDAT,
            }),
            authAdminToken
          );

    expect(responseTransfer.status).toBe(200);

    const responseReasonUpdate = await addAuthHeaders(
      request.put(endpointUpdateReason).send({
        description: DESCRIPTIONPRODUCT + '12221'
      }),
      authAdminToken
    );

  expect(responseReasonUpdate.status).toBe(200);
  });

  it('Error: Update Reason GHG after Product transfer with too short description', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "1234abc123",
          name: PRODUCTNAME + '1',
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferReason + '1234abc123').send({
              id:  "idReasonGHG1",
              ghgName: REASONNAME,
              reasonTitle: REASONTITLE,
              description: REASONDESCR,
              createdAt: REASONCREATEDAT,
            }),
            authAdminToken
          );

    expect(responseTransfer.status).toBe(200);

    const responseReasonUpdate = await addAuthHeaders(
      request.put(endpointUpdateReason).send({
        description: 'abc'
      }),
      authAdminToken
    );

  expect(responseReasonUpdate.status).toBe(400);
  });
});