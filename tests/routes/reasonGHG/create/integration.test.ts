jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Reason';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;
const REASONCREATEDAT = 1212434342312323;
const REASONNAME = 'Transportation';
const REASONTITLE = 'Transportation 1';
const REASONDESCR = "lorem ipsum dolor sit amet, lorem ipsum dolor sit amet,lorem ipsum dolor sit amet,"

describe('Integration: Test transfer reason ghg', () => {
  const endpointTransferReason = '/api/ghg/reason/transfer/first/relate/'
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Transfer Reason GHG after Product transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "ssdfsdfsfghfghfghdf3dddd34222344534534",
          name: PRODUCTNAME,
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferReason + 'ssdfsdfsfghfghfghdf3dddd34222344534534').send({
              id:  "123456785555590123456fffff7890",
              ghgName: REASONNAME,
              reasonTitle: REASONTITLE,
              description: REASONDESCR,
              createdAt: REASONCREATEDAT,
            }),
            authAdminToken
          );

    expect(responseTransfer.status).toBe(200);
  });
});