jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Total Update';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;
const TOTALCREATEDAT = 1212434342312323;
const GHGPERKG = 10;


describe('Integration: Test update of total ghg', () => {
  const endpointTransferTotal = '/api/ghg/total/transfer/first/relate/'
  const endpointUpdateTotal = '/api/general/update/fields/totalIdToUpdate123abc/total';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Update Total GHG after Product transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "uppdateId123abcIdUpdate",
          name: PRODUCTNAME,
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferTotal + 'uppdateId123abcIdUpdate').send({
              id:  "totalIdToUpdate123abc",
              ghgPerKG: GHGPERKG,
              createdAt: TOTALCREATEDAT,
            }),
            authAdminToken
          );

    expect(responseTransfer.status).toBe(200);

    const responseUpdate = await addAuthHeaders(
        request.put(endpointUpdateTotal).send({
          ghgPerKG: 10,
        }),
        authAdminToken
      );

    expect(responseUpdate.status).toBe(200);
  });

  it('Error: Update Total GHG after Product transfer wrong auth token ', async () => {

    const responseUpdate = await addAuthHeaders(
        request.put(endpointUpdateTotal).send({
          ghgPerKG: 10,
        }),
        '123'
      );

    expect(responseUpdate.status).toBe(403);
  });

});