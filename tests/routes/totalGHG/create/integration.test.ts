jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Total';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;
const TOTALCREATEDAT = 1212434342312323;
const GHGPERKG = 10;


describe('Integration: Test transfer total ghg', () => {
  const endpointTransferTotal = '/api/ghg/total/transfer/first/relate/'
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Transfer Total GHG after Product transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "121212243232dfvdfdf",
          name: PRODUCTNAME,
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferTotal + '121212243232dfvdfdf').send({
              id:  "123456785555590123456fffff7890",
              ghgPerKG: GHGPERKG,
              createdAt: TOTALCREATEDAT,
            }),
            authAdminToken
          );

    expect(responseTransfer.status).toBe(200);
  });


  it('Error: Transfer Total GHG with same id', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "ssdfsdfsdf3ddddddddd34344534534",
          name: PRODUCTNAME+"1",
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    await addAuthHeaders(
      request.post(endpointTransferTotal + '121212243232dfvdfdf').send({
        id:  "123456783434345555590123456fffff7890",
        ghgPerKG: GHGPERKG,
        createdAt: TOTALCREATEDAT,
      }),
      authAdminToken
    );

      const responseTransfer = await addAuthHeaders(
        request.post(endpointTransferTotal + '121212243232dfvdfdf').send({
          id:  "123456783434345555590123456fffff7890",
          ghgPerKG: GHGPERKG,
          createdAt: TOTALCREATEDAT,
        }),
        authAdminToken
      );
    
    expect(responseTransfer.status).toBe(406);
  });
});