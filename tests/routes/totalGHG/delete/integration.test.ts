jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Total Delete';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;
const TOTALCREATEDAT = 1212434342312323;
const GHGPERKG = 10;


describe('Integration: Test transfer total ghg', () => {
  const endpointTransferTotal = '/api/ghg/total/transfer/first/relate/';
  const endpointDeleteTotal = '/api/general/delete/node/123456785555590123456fffff7890/total';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Transfer Total GHG after Product transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "121212243232dfvdfdf",
          name: PRODUCTNAME,
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferTotal + '121212243232dfvdfdf').send({
              id:  "123456785555590123456fffff7890",
              ghgPerKG: GHGPERKG,
              createdAt: TOTALCREATEDAT,
            }),
            authAdminToken
          );

    expect(responseTransfer.status).toBe(200);

    const responseProductDelete = await addAuthHeaders(
        request.delete(endpointDeleteTotal).send({}),
        authAdminToken
      );

    expect(responseProductDelete.status).toBe(200);
  });
});