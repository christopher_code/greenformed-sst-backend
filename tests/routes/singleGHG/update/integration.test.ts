jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Single Update';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;
const GHGNAME = 'GHG Name';
const emissionPerKG = 10;

describe('Integration: Update transfer single ghg', () => {
  const endpointTransferSingle = '/api/ghg/single/transfer/first/relate/'
  const endpointUpdateSingle = '/api/general/update/fields/idUpdate123abc/single';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Update Single GHG Data after Product transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "idSingleGHGData",
          name: PRODUCTNAME,
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferSingle + 'idSingleGHGData').send({
              id:  "idUpdate123abc",
              ghgName: GHGNAME,
              emissionPerKG: emissionPerKG,
              createdAt: 12121,
            }),
            authAdminToken
          );

      expect(responseTransfer.status).toBe(200);

      const responseSingleUpdate = await addAuthHeaders(
        request.put(endpointUpdateSingle).send({
          emissionPerKG: 1
        }),
        authAdminToken
      );
  
      expect(responseSingleUpdate.status).toBe(200);
    });

    it('Error: Update Single GHG Data after Product transfer with wrong data type', async () => {
      const responseSingleUpdate = await addAuthHeaders(
        request.put(endpointUpdateSingle).send({
          emissionPerKG: 'abc'
        }),
        authAdminToken
      );
  
      expect(responseSingleUpdate.status).toBe(400);
    });
});
