jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Single';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;
const GHGNAME = 'GHG Name';
const emissionPerKG = 10;

describe('Integration: Test deletion of single ghg', () => {
  const endpointTransferSingle = '/api/ghg/single/transfer/first/relate/';
  const endpointDeleteSingle = '/api/general/delete/node/123456785555590123456fffff7890/single';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Delete Single GHG Data after Product transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "ssdfsdfsdf33434343dddd34344534534",
          name: PRODUCTNAME,
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferSingle + 'ssdfsdfsdf33434343dddd34344534534').send({
              id:  "123456785555590123456fffff7890",
              ghgName: GHGNAME,
              emissionPerKG: emissionPerKG,
              createdAt: 12121,
            }),
            authAdminToken
          );

    expect(responseTransfer.status).toBe(200);
    
    const responseProductDelete = await addAuthHeaders(
        request.delete(endpointDeleteSingle).send({}),
        authAdminToken
      );

    expect(responseProductDelete.status).toBe(200);
  });

});
