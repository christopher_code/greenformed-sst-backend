jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Seller Delete';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;
const LINK = 'kskdksfksdfkdfk.de';
const NAME = 'jsfjsdjfsjdfjsdjfsjdfs';

describe('Integration: Test transfer seller', () => {
  const endpointTransferSeller = '/api/seller/transfer/first/relate/';
  const endpointDeleteSeller = '/api/general/delete/node/123456785555590123456fffff7890/seller';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

  it('Delete Seller after Product transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "ssdfsdfsdf333333dddd34344534534",
          name: PRODUCTNAME,
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferSeller + 'ssdfsdfsdf333333dddd34344534534').send({
              id:  "123456785555590123456fffff7890",
              name: NAME,
              link: LINK,
              createdAt: 12121,
            }),
            authAdminToken
          );

    expect(responseTransfer.status).toBe(200);

    const responseProductDelete = await addAuthHeaders(
        request.delete(endpointDeleteSeller).send({}),
        authAdminToken
      );

    expect(responseProductDelete.status).toBe(200);

  });
});