
jest.resetAllMocks();

import supertest from 'supertest';
import app, { neogma } from '../../../../src/app';
import { addAuthHeaders } from "../../../setUp/mock";
import { authAdminToken } from "../../../setup"; 

const PRODUCTNAME = 'Product Name Seller Update';
const DESCRIPTIONPRODUCT = "Product Description 121212121212121212";
const LINKTOBUY = "http.test.de";
const COSTPERPRODUCT = 12;
const CREATEDAT = 121121212121212;
const LINK = 'kskdksfksdfkdfk.de';
const NAME = 'jsfjsdjfsjdfjsdjfsjdfs';

describe('Integration: Test update seller', () => {
  const endpointTransferSeller = '/api/seller/transfer/first/relate/';
  const endpointUpdatSeller = '/api/general/update/fields/12updateIdSeller123abc/seller';
  const request = supertest(app);

  beforeAll(async () => {});

  afterAll(async () => {
    await neogma.queryRunner.run('match (n) detach delete n');
  });

  beforeEach(() => {});

it('Update Seller after Product transfer', async () => {
    const responseProductCreation = await addAuthHeaders(
      request.post('/api/product/transfer/first').send({
          id: "transferIdSeller",
          name: PRODUCTNAME,
          description: DESCRIPTIONPRODUCT,
          linkToBuy: LINKTOBUY,
          costPerProduct: COSTPERPRODUCT,
          createdAt: CREATEDAT,
      }),
      authAdminToken
    );

    expect(responseProductCreation.status).toBe(200);

    const responseTransfer = await addAuthHeaders(
            request.post(endpointTransferSeller + 'transferIdSeller').send({
              id:  "12updateIdSeller123abc",
              name: NAME + '1',
              link: LINK + '1',
              createdAt: 12121,
            }),
            authAdminToken
          );

    expect(responseTransfer.status).toBe(200);

    const responseSellerUpdate = await addAuthHeaders(
        request.put(endpointUpdatSeller).send({
            link: "new.link122122122"
        }),
        authAdminToken
      );
  
      expect(responseSellerUpdate.status).toBe(200);

  });
});