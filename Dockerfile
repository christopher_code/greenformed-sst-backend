FROM node:14

USER node

RUN mkdir -p /home/node/app && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./
COPY --chown=node:node . .

RUN npm install

RUN npx tsc

ENV PORT=8080
EXPOSE 8080

CMD [ "node", "build/server.js" ]