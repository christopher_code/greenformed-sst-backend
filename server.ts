import Logger from './src/middleware/LogFile';
import { port } from './config';
import app from './src/app';

app.listen(port, () => {
    console.log(`SST BE app listening at http://localhost:${port}`);
    Logger.info(`SST BE app listening at http://localhost:${port}`)
  })
  .on('error', (e: any) => Logger.error(e));
